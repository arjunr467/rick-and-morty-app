import 'package:flutter/material.dart';
import 'package:rickAndMorty/graphql/graphql_service.dart';
import 'package:rickAndMorty/graphql/models/data_response.dart';
import 'package:rickAndMorty/schema/get_characters_schema.dart';

class GetCharactersProvider {
  GetCharactersSchema _getCharactersSchema = GetCharactersSchema();

  Future<DataResponse> getCharactersList({int pageNumber}) async {
    try {
      final queryOptions = GraphQLService.makeQueryOptions(
        query: _getCharactersSchema.getCharacters,
        networkOnly: true,
        variables: {
          "pageNum": pageNumber,
        },
      );

      final graphQLResponse = await GraphQLService.performQuery(
        queryOptions: queryOptions,
      );

      if (graphQLResponse.hasData) {
        return DataResponse(
          data: graphQLResponse.data,
        );
      } else {
        return DataResponse(
          error: graphQLResponse.error.message,
        );
      }
    } catch (e) {
      debugPrint("GetCharactersProvider" + e.toString());
    }
    return DataResponse(
      error: "Some Error",
    );
  }
}
