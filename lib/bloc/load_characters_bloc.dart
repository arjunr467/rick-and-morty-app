import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rickAndMorty/bloc/load_characters_event.dart';
import 'package:rickAndMorty/bloc/load_characters_state.dart';
import 'package:rickAndMorty/models/character_model.dart';
import 'package:rickAndMorty/provider/get_characters_provider.dart';

class CharactersBloc extends Bloc<LoadCharactersEvent, LoadCharactersState> {
  GetCharactersProvider _getCharactersProvider = GetCharactersProvider();
  CharactersBloc() : super(ListLoading());

  @override
  Stream<LoadCharactersState> mapEventToState(
      LoadCharactersEvent event) async* {
    if (event is FetchCharacters) {
      yield ListLoading();
      final result = await _getCharactersProvider.getCharactersList(
        pageNumber: event.pageNumber,
      );

      if (result.hasData) {
        CharacterModel characterModel = CharacterModel.fromJson(result.data);
        if (characterModel != null) {
          yield ListLoaded(
            info: characterModel.characters.info,
            results: characterModel.characters.results,
          );
        }
      }
    }
  }
}
