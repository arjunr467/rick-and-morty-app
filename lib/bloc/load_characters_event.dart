import 'package:equatable/equatable.dart';

abstract class LoadCharactersEvent extends Equatable {
  const LoadCharactersEvent();
}

class FetchCharacters extends LoadCharactersEvent {
  final int pageNumber;

  FetchCharacters({this.pageNumber});

  @override
  List<Object> get props => [pageNumber];
}
