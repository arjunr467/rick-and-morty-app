import 'package:equatable/equatable.dart';
import 'package:rickAndMorty/models/character_model.dart';

abstract class LoadCharactersState extends Equatable {
  const LoadCharactersState();
}

class ListLoading extends LoadCharactersState {
  @override
  List<Object> get props => [];
}

class ListLoaded extends LoadCharactersState {
  final List<Result> results;
  final Info info;
  ListLoaded({
    this.info,
    this.results,
  });
  @override
  List<Object> get props => [results, info];
}

class ListLoadingFailed extends LoadCharactersState {
  final String message;
  ListLoadingFailed({this.message});

  @override
  List<Object> get props => [message];
}
