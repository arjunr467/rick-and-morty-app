import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
part 'character_model.g.dart';

@JsonSerializable()
class Info extends Equatable {
  final int count;
  final int pages;
  final int next;
  final int prev;
  Info({
    this.count,
    this.pages,
    this.next,
    this.prev,
  });
  @override
  List<Object> get props => [count, pages, next, prev];

  factory Info.fromJson(Map<String, dynamic> json) => _$InfoFromJson(json);

  Map<String, dynamic> toJson() => _$InfoToJson(this);
}

@JsonSerializable()
class Location extends Equatable {
  final String name;
  Location({this.name});

  @override
  List<Object> get props => [name];

  factory Location.fromJson(Map<String, dynamic> json) =>
      _$LocationFromJson(json);

  Map<String, dynamic> toJson() => _$LocationToJson(this);
}

@JsonSerializable()
class Result extends Equatable {
  final String id;
  final String name;
  final String status;
  final String species;
  final String type;
  final String gender;
  final String image;
  final Location location;
  final DateTime created;

  Result({
    this.id,
    this.created,
    this.gender,
    this.name,
    this.image,
    this.location,
    this.species,
    this.status,
    this.type,
  });

  @override
  List<Object> get props => [
        id,
        created,
        gender,
        name,
        image,
        location,
        species,
        status,
        type,
      ];

  factory Result.fromJson(Map<String, dynamic> json) => _$ResultFromJson(json);

  Map<String, dynamic> toJson() => _$ResultToJson(this);
}

@JsonSerializable()
class Characters extends Equatable{
  final Info info;
  final List<Result> results;

  Characters({this.info,this.results});

  @override
  List<Object> get props => [info,results];

  factory Characters.fromJson(Map<String,dynamic> json) => _$CharactersFromJson(json);

  Map<String,dynamic> toJson() => _$CharactersToJson(this);
}

@JsonSerializable()
class CharacterModel extends Equatable{
  final Characters characters;
  CharacterModel({this.characters});
  @override
  List<Object> get props => [characters];

  factory CharacterModel.fromJson(Map<String,dynamic> json) => _$CharacterModelFromJson(json);

  Map<String,dynamic> toJson() => _$CharacterModelToJson(this);
}