import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rickAndMorty/bloc/load_characters_bloc.dart';
import 'package:rickAndMorty/bloc/load_characters_event.dart';
import 'package:rickAndMorty/bloc/load_characters_state.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int pageNumber = 1;
  int maxPages = 130;

  @override
  void initState() {
    super.initState();
    BlocProvider.of<CharactersBloc>(context).add(
      FetchCharacters(
        pageNumber: pageNumber,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Rick And Morty"),
        centerTitle: true,
        backgroundColor: Colors.lightBlueAccent[200],
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 0.0),
        child: BlocBuilder<CharactersBloc, LoadCharactersState>(
          builder: (context, state) {
            if (state is ListLoading) {
              return Center(
                child: CupertinoActivityIndicator(),
              );
            } else if (state is ListLoaded) {
              maxPages = state.info.pages;
              if (state.results.isNotEmpty) {
                return SingleChildScrollView(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 12.0),
                        child: Row(
                          children: [
                            Expanded(
                              child: FlatButton(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(22.0),
                                ),
                                color: Colors.lightBlueAccent,
                                onPressed: pageNumber == 1
                                    ? null
                                    : () {
                                        setState(() {
                                          pageNumber = pageNumber - 1;
                                        });
                                        BlocProvider.of<CharactersBloc>(context)
                                            .add(
                                          FetchCharacters(
                                            pageNumber: pageNumber,
                                          ),
                                        );
                                      },
                                child: Text(
                                  "PREV",
                                  style: TextStyle(color: Colors.black),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Center(
                                child: Container(
                                  padding: EdgeInsets.all(10.0),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(22.0),
                                    color: Colors.grey,
                                  ),
                                  child: Text(
                                    pageNumber.toString(),
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: FlatButton(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(22.0),
                                ),
                                color: Colors.lightBlueAccent,
                                onPressed: pageNumber < maxPages
                                    ? () {
                                        setState(() {
                                          pageNumber = pageNumber + 1;
                                        });
                                        BlocProvider.of<CharactersBloc>(context)
                                            .add(
                                          FetchCharacters(
                                            pageNumber: pageNumber,
                                          ),
                                        );
                                      }
                                    : null,
                                child: Text(
                                  "NEXT",
                                  style: TextStyle(color: Colors.black),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      GridView.builder(
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2, childAspectRatio: 0.56),
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: state.results.length,
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: () {
                              showDialog(
                                  context: context,
                                  barrierDismissible: false,
                                  useSafeArea: true,
                                  builder: (childContext) {
                                    return SimpleDialog(
                                      backgroundColor: Colors.transparent,
                                      children: [
                                        ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(22.0),
                                          child: Image.network(
                                            state.results[index].image,
                                            fit: BoxFit.contain,
                                          ),
                                        ),
                                      ],
                                    );
                                  });
                            },
                            child: Card(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(22.0),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(22.0),
                                      topRight: Radius.circular(22.0),
                                    ),
                                    child: Image.network(
                                      state.results[index].image,
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "Name: ${state.results[index].name}",
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(fontSize: 13.0),
                                          ),
                                          Text(
                                            "Status : ${state.results[index].status}",
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(fontSize: 13.0),
                                          ),
                                          Text(
                                            "Species: ${state.results[index].species}",
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(fontSize: 13.0),
                                          ),
                                          Text(
                                            "Gender: ${state.results[index].gender}",
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(fontSize: 13.0),
                                          ),
                                          Text(
                                            "Location: ${state.results[index].location.name}",
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(fontSize: 13.0),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                );
              } else {
                return Center(
                  child: Text("No Data Found"),
                );
              }
            } else {
              return Center(
                child: Text("No Data Found"),
              );
            }
          },
        ),
      ),
    );
  }
}
