class GetCharactersSchema{
  String getCharacters = """
  query characters(\$pageNum:Int!){
  characters(page:\$pageNum){
    info{
      count
      pages
      next
      prev
    }
    results{
      id
      name
      status
      species
      type
      gender
      image
      location{
        name
      }
      created
    }
  }
}
  """;
}