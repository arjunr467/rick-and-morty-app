import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rickAndMorty/bloc/load_characters_bloc.dart';
import 'package:rickAndMorty/screens/home_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider<CharactersBloc>(
      create: (context) => CharactersBloc(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Rick And Morty App',
        theme: ThemeData.dark(),
        home: HomePage(),
      ),
    );
  }
}
