import 'package:graphql/client.dart';

class ErrorInfo {
  final String message;
  final int statusCode;
  final String type;

  ErrorInfo({
    this.message,
    this.statusCode,
    this.type,
  });

  factory ErrorInfo.fromJson({
    Map<String, dynamic> jsonData,
    OperationException operationException,
  }) {
    if (operationException != null) {
      if (operationException.graphqlErrors.isNotEmpty) {
        return ErrorInfo.fromJson(
          jsonData: operationException.graphqlErrors.first.raw,
        );
      } else if (operationException.clientException != null) {
        return ErrorInfo(
          message: operationException.clientException.message.trim().isEmpty
              ? 'Some error occurred'
              : operationException.clientException.message.trim(),
          statusCode: 500,
          type: 'SOME_ERROR',
        );
      }

      return ErrorInfo(
        message: 'Some error occurred',
        statusCode: 500,
        type: 'SOME_ERROR',
      );
    } else {
      return ErrorInfo(
        message: jsonData.containsKey("message") ? jsonData["message"] : null,
        statusCode:
            jsonData.containsKey("statusCode") ? jsonData["statusCode"] : null,
        type: jsonData.containsKey("type") ? jsonData["type"] : null,
      );
    }
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> jsonData = {};

    if (message != null) {
      jsonData["message"] = message;
    }

    if (statusCode != null) {
      jsonData["statusCode"] = statusCode;
    }

    if (type != null) {
      jsonData["type"] = type;
    }

    return jsonData;
  }
}
