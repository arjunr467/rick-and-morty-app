class DataResponse {
  final String error;
  final dynamic data;

  bool get hasData => data != null;

  DataResponse({
    this.error,
    this.data,
  });
}
