class ErrorTextConversion {
  String convertErrorText(String errorCode) {
    switch (errorCode) {
      case "BAD_REQUEST":
        return "it is a BAD REQUEST";
        break;
      case "UNAUTHORIZED":
        return "UNAUTHORIZED acess";
        break;
      case "FORBIDDEN":
        return "FORBIDDEN";
        break;
      case "NOT_FOUND":
        return "Data NOT FOUND";
        break;
      case "INVALID_EMAIL":
        return "entered EMAIL address is invalid";
        break;
      case "INVALID_PHONE":
        return "entered PHONE number is invalid";
        break;
      case "FAILED":
        return "sorry it is a FAILED attempt";
        break;
      case "SOME_ERROR":
        return "having SOME ERROR";
        break;
      case "NO_CHANGES":
        return "NO CHANGES";
        break;
      case "ALREADY_EXIST":
        return "entered data ALREADY EXISTS";
        break;
      case "EMAIL_ALREADY_EXIST":
        return "entered EMAIL ALREADY EXISTS please enter a new email";
        break;
      case "PHONE_ALREADY_EXIST":
        return "entered PHONE ALREADY EXISTS please change your number";
        break;
      case "ALREADY_VERIFIED":
        return "entered data is ALREADY VERIFIED";
        break;
      case "UPLOAD_FAILED":
        return "UPLOAD FAILED please try again";
        break;
      case "INVALID_FILE":
        return "you have uploaded an INVALID FILE";
        break;
      case "LESS_WALLET_POINT":
        return "you have LESS WALLET POINTS";
        break;
      case "TASK_NOT_FOUND":
        return "TASK NOT FOUND";
        break;
      case "WRONG_PASSWORD":
        return "WRONG PASSWORD please try again";
        break;
      case "OTP_EXPIRED":
        return "OTP EXPIRED please try again";
        break;
      case "ALREADY_PAID":
        return "ALREADY PAID for the subscription";
        break;
      case "ALREADY_CREATED":
        return "ALREADY CREATED";
        break;
      case "ALREADY_IN_USE":
        return "ALREADY IN USE";
        break;
      default:
        return "Unknown issue";
        break;
    }
  }
}
