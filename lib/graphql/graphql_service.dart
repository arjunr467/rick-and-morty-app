import 'dart:io';

import 'package:flutter/material.dart';
import 'package:graphql/client.dart';
import 'package:graphql/internal.dart';
import 'package:path_provider/path_provider.dart';
import 'models/error_info.dart';
import 'models/graphql_response.dart';

class GraphQLService {
  static HttpLink _httpLink = HttpLink(
    uri: "https://rickandmortyapi.com/graphql",
  );

  // static AuthLink _authLink = AuthLink(
  //   getToken: () async =>
  //       '',
  // );

  // static Link _link = _authLink.concat(_httpLink);
  static Link _link = _httpLink;

  static GraphQLClient graphQLClient = GraphQLClient(
    link: _link,
    cache: InMemoryCache(storagePrefix: _cacheStoragePath()),
  );

  static Future<String> _cacheStoragePath() async {
    final appDocumentsDirectory = await getApplicationDocumentsDirectory();
    return appDocumentsDirectory.path;
  }

  static ValueNotifier<GraphQLClient> valueNotifierClient =
      ValueNotifier(graphQLClient);

  static resetCache() async {
    graphQLClient.cache?.reset();
    await graphQLClient.cache?.save();
  }

  static Future<GraphQLResponse> performQuery({
    @required QueryOptions queryOptions,
  }) async {
    final result = await graphQLClient.query(queryOptions);

    if (result.hasException) {
      return GraphQLResponse(
        error: ErrorInfo.fromJson(operationException: result.exception),
      );
    } else {
      return GraphQLResponse(
        data: result.data,
      );
    }
  }

  static Future<GraphQLResponse> performMutation({
    @required MutationOptions mutationOptions,
  }) async {
    final result = await graphQLClient.mutate(mutationOptions);

    if (result.hasException) {
      return GraphQLResponse(
        error: ErrorInfo.fromJson(operationException: result.exception),
      );
    } else {
      return GraphQLResponse(
        data: result.data,
      );
    }
  }

  static ObservableQuery getWatchQuery({
    @required documentNode,
    Map<String, dynamic> variables,
  }) {
    return graphQLClient.watchQuery(
      WatchQueryOptions(
        documentNode: documentNode,
        variables: variables,
      ),
    );
  }

  static QueryOptions makeQueryOptions({
    @required String query,
    Map<String, dynamic> variables,
    bool networkOnly = false,
  }) {
    return QueryOptions(
      documentNode: gql(query),
      variables: variables,
      fetchPolicy:
          networkOnly ? FetchPolicy.networkOnly : FetchPolicy.cacheFirst,
    );
  }

  static MutationOptions makeMutationOptions({
    @required String mutation,
    Map<String, dynamic> variables,
    Function(Cache, QueryResult) onUpdate,
    Function(OperationException) onError,
  }) {
    return MutationOptions(
      documentNode: gql(mutation),
      variables: variables,
      update: onUpdate,
      onError: onError,
    );
  }
}
